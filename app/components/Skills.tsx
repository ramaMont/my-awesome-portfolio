'use client';

import React from 'react';
import { motion } from 'framer-motion';

type SkillProps = {
    name: string;
    x: string;
    y: string;
}

const Skill : React.FC<SkillProps> = ({ name, x, y }) => (
  <motion.div
    className='flex items-center justify-center rounded-full font-semibold bg-dark text-light shadow-dark py-3 px-6 cursor-pointer absolute dark:bg-light dark:text-dark lg:py-2 lg:px-4 md:text-sm md:py-1.5 md:px-3 xs:bg-transparent xs:dark:bg-transparent xs:text-dark xs:dark:text-light xs:text-sm'
    whileHover={ { scale: 1.1 } }
    initial={ { x: 0, y: 0 } }
    whileInView={ { x, y, transition: { duration: 1.5 } } }
    viewport={ { once:true } }
  >
    { name }
  </motion.div>
);

const Skills = () => {
  return (
    <div>
      <h2 className='font-bold text-8xl w-full text-center md:text-6xl '>Skills</h2>
      <div className='w-full h-[60vh] relative flex items-center justify-center rounded-full bg-circularLight dark:bg-circularDark xs:h-[50vh] lg:bg-circularLightLg lg:dark:bg-circularDarkLg md:bg-circularLightMd md:dark:bg-circularDarkMd sm:bg-circularLightSm sm:dark:bg-circularDarkSm'>
        <motion.div
          className='flex items-center justify-center rounded-full font-semibold bg-dark text-light p-8 shadow-dark cursor-pointer dark:bg-light dark:text-dark lg:p-6 md:p-4 xs:text-xs xs:p-2'
          whileHover={ { scale: 1.1 } }
        >
            Fullstack
        </motion.div>
        <Skill name='React' x="-20vw" y="12vh" />
        <Skill name='Javascript' x="-5vw" y="-10vh" />
        <Skill name='Next.js' x="20vw" y="5vh" />
        <Skill name='Node.js' x="-20vw" y="-15vh" />
        <Skill name='HTML' x="27vw" y="-5vh" />
        <Skill name='CSS' x="10vw" y="-13vh" />
        <Skill name='Tailwind CSS' x="18vw" y="19vh" />
        <Skill name='Python' x="18vw" y="-18vh" />
        <Skill name='Java' x="10vw" y="25vh" />
        <Skill name='Ruby' x="-10vw" y="23vh" />
        <Skill name='Linux' x="-15vw" y="5vh" />
        <Skill name='GCP' x="5vw" y="10vh" />
        <Skill name='TDD/BDD' x="0vw" y="17vh" />
        <Skill name='CI/CD' x="-4vw" y="-20vh" />
      </div>
    </div>
  );
};

export default Skills;
