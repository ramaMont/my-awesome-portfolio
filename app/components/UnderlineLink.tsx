'use client';
import React from 'react';
import Link from 'next/link';
import { usePathname } from 'next/navigation';

interface UnderlineLinkProps {
    children: React.ReactNode;
    href: string;
}

const UnderlineLink : React.FC<UnderlineLinkProps> = ({ children, href }) => {
  const pathname = usePathname();
  return (
    <Link href={ href } className='relative group'>
      { children }
      <span className={ `h-1 inline-block  absolute left-0 -bottom-0.5 bg-dark ${pathname === href ? 'w-full' : 'w-0'}
      group-hover:w-full transition-[width] ease duration-300 dark:bg-light`  }
      >
        &nbsp;
      </span>
    </Link>
  );
};

export default UnderlineLink;
