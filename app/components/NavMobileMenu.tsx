'use client';

import React from 'react';
import HamburgerButton from './HamburgerButton';
import ModalUnderlineLink from './ModalUnderlineLink';
import InteractiveLogo from './InteractiveLogo';
import Image from 'next/image';
import ThemeSwitch from './ThemeSwitch';
import { motion } from 'framer-motion';

const NavMobileMenu = () => {
  const [isMenuOpen, setIsMenuOpen] = React.useState(false);
  const closeMenu = () => setIsMenuOpen(false);
  const modalRef = React.useRef(null);
  const buttonRef = React.useRef(null);

  React.useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (modalRef.current && !(modalRef.current as HTMLElement).contains(event.target as Node) && buttonRef.current && !(buttonRef.current as HTMLElement).contains(event.target as Node)){
        closeMenu();
      }
    };

    if (isMenuOpen) {
      document.addEventListener('mousedown', handleClickOutside);
    } else {
      document.removeEventListener('mousedown', handleClickOutside);
    }

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [isMenuOpen]);

  return (
    <div className='hidden lg:flex h-9'>
      <HamburgerButton reference={ buttonRef } onClick={ () => setIsMenuOpen((prev) => !prev) } isMenuOpen={ isMenuOpen } />
      {
        isMenuOpen && (
          <motion.div
            ref={ modalRef }
            className='min-w-[70vw] flex flex-col justify-between items-center fixed top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 bg-dark/90 dark:bg-light/70 rounded-lg backdrop-blur-md py-32 z-30 gap-10'
            initial={ { scale: 0, opacity: 0, x:'-50%', y:'-50%' } }
            animate={ { scale: 1, opacity: 1 } }
          >
            <nav className='flex flex-col justify-between items-center gap-6'>
              <ModalUnderlineLink onClick={ closeMenu } href="/">Home</ModalUnderlineLink>
              <ModalUnderlineLink onClick={ closeMenu } href="/about">About</ModalUnderlineLink>
            </nav>
            <nav className='flex justify-between items-center gap-1 w-3/5'>
              <InteractiveLogo href="https://linkedin.com/in/ramiro-montiel" >
                <Image src="/linkedin.png" alt="linkedin" width={ 32 } height={ 32 }/>
              </InteractiveLogo>
              <InteractiveLogo href="https://github.com/ramaMont">
                <Image src="/github.png" alt="github" width={ 32 } height={ 32 } className='invert dark:filter dark:invert-0'/>
              </InteractiveLogo>
              <InteractiveLogo href="https://gitlab.com/ramaMont">
                <Image src="/gitlab.png" alt="gitlab" width={ 32 } height={ 32 }/>
              </InteractiveLogo>
              <ThemeSwitch inModal />
            </nav>
          </motion.div>
        )
      }
    </div>
  );
};

export default NavMobileMenu;
