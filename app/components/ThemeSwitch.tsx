'use client';

import React from 'react';
import { motion } from 'framer-motion';
import Image from 'next/image';
import { useTheme } from 'next-themes';
import sun from '../../public/icons/sun.svg';
import moon from '../../public/icons/moon.svg';

type ThemeSwitchProps = {
  inModal?: boolean;
}

const ThemeSwitch : React.FC<ThemeSwitchProps> = ({ inModal = false }) => {
  const { setTheme, resolvedTheme } = useTheme();

  const handleClick = () => {
    setTheme(resolvedTheme === 'dark' ? 'light' : 'dark');
  };

  return (
    <button
      onClick={ handleClick }
      className="relative focus:outline-none transition duration-300 ease-in-out"
    >
      <motion.div
        whileHover={ { scale: 1.1 } }
        whileTap={ { scale: 0.9 } }
      >
        <motion.div
          initial={ { opacity: 0, scale: 0 } }
          animate={ { opacity: 1, scale: 1 } }
          transition={ { duration: 0.5 } }
          className="relative w-9 h-9"
        >
          <Image
            src={ resolvedTheme === 'dark' ? sun : moon }
            alt="sun or moon"
            height={ 36 }
            width={ 36 }
            className={ `object-cover w-full h-full ${inModal ? 'invert dark:filter dark:invert-0' : 'dark:invert'}` }
          />
        </motion.div>
      </motion.div>
    </button>
  );
};

export default ThemeSwitch;
