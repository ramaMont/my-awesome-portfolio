'use client';

import React from 'react';
import { usePathname } from 'next/navigation';
import { AnimatePresence, motion } from 'framer-motion';
import FrozenRoute from './FrozenRoute';

type PageAnimatePresenceProps = {
    children: React.ReactNode;
}

const PageAnimatePresence : React.FC<PageAnimatePresenceProps> = ({ children }) => {
  const pathname = usePathname();

  return (
    <AnimatePresence mode="wait">
      <motion.div key={ pathname }>
        <FrozenRoute>{ children }</FrozenRoute>
      </motion.div>
    </AnimatePresence>
  );
};

export default PageAnimatePresence;
