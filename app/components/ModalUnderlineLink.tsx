'use client';
import React from 'react';
import Link from 'next/link';
import { usePathname } from 'next/navigation';

interface ModalUnderlineLinkProps {
    children: React.ReactNode;
    href: string;
    onClick?: () => void;
}

const ModalUnderlineLink : React.FC<ModalUnderlineLinkProps> = ({ children, href, onClick = () => {} }) => {
  const pathname = usePathname();
  return (
    <Link href={ href } className='relative group text-light dark:text-dark' onClick={ onClick }>
      { children }
      <span className={ `h-1 inline-block  absolute left-0 -bottom-0.5 bg-dark ${pathname === href ? 'w-full' : 'w-0'}
      group-hover:w-full transition-[width] ease duration-300 bg-light dark:bg-dark`  }
      >
        &nbsp;
      </span>
    </Link>
  );
};

export default ModalUnderlineLink;
