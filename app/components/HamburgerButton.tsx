import React from 'react';

type HamburgerButtonProps = {
    onClick: () => void;
    isMenuOpen: boolean;
    reference: React.RefObject<HTMLButtonElement>;
};

const HamburgerButton : React.FC<HamburgerButtonProps> = ({ onClick, isMenuOpen, reference }) => (
  <button onClick={ onClick } ref={ reference }>
    <span className={ `bg-dark dark:bg-light block transition-all duration-300 ease-out h-0.5 w-6 rounded-sm ${isMenuOpen ? 'rotate-45 translate-y-1' : '-translate-y-0.5'}` } />
    <span className={ `bg-dark dark:bg-light block transition-all duration-300 ease-out h-0.5 w-6 rounded-sm my-0.5 ${isMenuOpen ? 'opacity-0' : 'opacity-100'}` } />
    <span className={ `bg-dark dark:bg-light block transition-all duration-300 ease-out h-0.5 w-6 rounded-sm ${isMenuOpen ? '-rotate-45 -translate-y-1' : 'translate-y-0.5'}` } />
  </button>
);

export default HamburgerButton;
