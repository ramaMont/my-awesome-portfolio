'use client';

import { useInView, useMotionValue, useSpring } from 'framer-motion';
import React, { useRef } from 'react';

type AnimatedNumberProps = {
    numberValue: number;
}

const AnimatedNumber : React.FC<AnimatedNumberProps> = ({ numberValue }) => {
  const ref = useRef<HTMLSpanElement>(null);
  const motionValue = useMotionValue(0);
  const springValue = useSpring(motionValue, { duration: 2000 });
  const isInView = useInView(ref, { once: true });

  React.useEffect(() => {
    if(isInView){
      motionValue.set(numberValue);
    }
  }, [isInView, motionValue, numberValue]);

  React.useEffect(() => {
    springValue.on('change', (latest : number) => {
      if(ref.current && Number(latest.toFixed(0)) <= numberValue){
        ref.current.textContent = latest.toFixed(0);
      }
    });
  }, [springValue, numberValue]);

  return (
    <span className='inline-block text-7xl font-bold md:text-6xl sm:text-5xl xs:text-4xl'>
      +<span ref={ ref } />
    </ span>
  );
};

export default AnimatedNumber;
