'use client';

import React from 'react';
import { motion, useScroll } from 'framer-motion';
import LiIcon from './LiIcon';
import { highSchoolText, universityText } from '../constants/aboutPage';

type SchoolEducationProps = {
  type: string;
  time: string;
  place: string;
  placeLink?: string;
  info: string;
}

const SchoolEducation : React.FC<SchoolEducationProps> = ({ type, time, place, placeLink = '', info }) => {
  const ref = React.useRef(null);
  return(
    <li ref={ ref } className='w-3/5 mx-auto flex flex-col justify-between md:w-4/5'>
      <LiIcon reference={ ref } />
      <motion.div
        initial={ { y: 50 } }
        whileInView={ { y: 0 } }
        transition={ { duration: 0.5, type: 'spring' } }
      >
        <h3 className='capitalize font-bold text-2xl sm:text-xl xs:text-lg'>
          { type }&nbsp;
          <a
            href={ placeLink }
            target='_blank'
            className={ `text-primary capitalize ${ !placeLink && 'pointer-events-none' }` }
          >
            @{ place }
          </a>
        </h3>
        <span
          className='text-dark/75 font-medium capitalize dark:text-light/75 xs:text-sm'
        >
          { time }
        </span>
        <p className='font-medium w-full md:text-sm'>
          { info }
        </p>
      </motion.div>
    </li>
  );};

const Education = () => {
  const ref = React.useRef(null);
  const { scrollYProgress } = useScroll({
    target: ref,
    offset: ['start end', 'end start']
  });

  return (
    <div className='flex flex-col mb-40 gap-8'>
      <h2 className='font-bold text-8xl w-full text-center md:text-6xl xs:text-4xl'>
        Education
      </h2>
      <div className='w-3/4 mx-auto relative'>
        <div ref={ ref } className='w-3/4 mx-auto relative lg:w-[90%] md:w-full'/>
        <motion.div
          className='absolute left-9 top-0 w-1 h-full bg-dark origin-top dark:bg-light md:w-[2px] md:left-[30px] xs:left-[20px]'
          style={ { scaleY: scrollYProgress } }
        />
        <ul className='w-full flex flex-col items-start justify-between ml-4 gap-8 xs:ml-2'>
          <SchoolEducation
            place="Universidad de buenos aires - Facultad de Ingeniería"
            placeLink="https://fi.uba.ar/"
            type="Bachelor in Computer Science"
            time="2016 - 2023"
            info={ universityText }
          />
          <SchoolEducation
            place="Escuela Técnica Nro. 28"
            type="Electronic Technician"
            placeLink="https://www.et28.net/expotecnica-2020/"
            time="2010 - 2015"
            info={ highSchoolText }
          />
        </ul>
      </div>
    </div>
  );
};

export default Education;
