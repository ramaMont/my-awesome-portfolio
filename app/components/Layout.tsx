import React from 'react';

type LayoutProps = {
    children: React.ReactNode;
    className?: string;
}

const Layout : React.FC<LayoutProps> =  ({ children, className='' }) => (
  <div className={ `w-full flex z-0 px-32 pt-4 xl:px-24 lg:px-16 md:px-12 sm:px-8 justify-center ${className}` }>
    <div className='max-w-[1300px]'>
      { children }
    </div>
  </div>
);

export default Layout;
