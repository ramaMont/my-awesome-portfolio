'use client';

import React from 'react';
import { motion, useScroll } from 'framer-motion';
import LiIcon from './LiIcon';
import { amalgamaExperience, freelancerExperience, nacionExperience, unagiExperience, voicesfyExperience } from '../constants/aboutPage';

type CompanyExperienceProps = {
    company: string;
    role: string;
    duration: string;
    description: string;
    location: string;
    companyLink?: string;
}

const CompanyExperience : React.FC<CompanyExperienceProps> = ({ company, role, duration, description, location, companyLink = '' }) => {
  const ref = React.useRef(null);
  return(
    <li ref={ ref } className='w-3/5 mx-auto flex flex-col justify-between md:w-4/5'>
      <LiIcon reference={ ref } />
      <motion.div
        initial={ { y: 50 } }
        whileInView={ { y: 0 } }
        transition={ { duration: 0.5, type: 'spring' } }
      >
        <h3 className='capitalize font-bold text-2xl sm:text-xl xs:text-lg'>
          { role }&nbsp;
          <a
            href={ companyLink }
            target='_blank'
            className={ `text-primary capitalize ${ !companyLink && 'pointer-events-none' }` }
          >
            @{ company }
          </a>
        </h3>
        <span
          className='text-dark/75 font-medium capitalize dark:text-light/75 xs:text-sm'
        >
          { duration } | { location }
        </span>
        <p className='font-medium w-full md:text-sm'>
          { description }
        </p>
      </motion.div>
    </li>
  );};

const Experience = () => {
  const ref = React.useRef(null);
  const { scrollYProgress } = useScroll({
    target: ref,
    offset: ['start end', 'end start']
  });

  return (
    <div className='flex flex-col gap-8'>
      <h2 className='font-bold text-8xl w-full text-center md:text-6xl xs:text-4xl'>
        Experience
      </h2>
      <div className='w-3/4 mx-auto relative'>
        <div ref={ ref } className='w-3/4 mx-auto relative lg:w-[90%] md:w-full'/>
        <motion.div
          className='absolute left-9 top-0 w-1 h-full bg-dark origin-top dark:bg-light md:w-[2px] md:left-[30px] xs:left-[20px]'
          style={ { scaleY: scrollYProgress } }
        />
        <ul className='w-full flex flex-col items-start justify-between ml-4 gap-8 xs:ml-2'>
          <CompanyExperience
            company="Voicesfy"
            companyLink="https://voicesfy.com/"
            role="Tech Lead"
            duration="July 2024 - Present"
            location="Remote"
            description={ voicesfyExperience }
          />
          <CompanyExperience
            company="Unagi"
            companyLink="https://unagisoftware.com/"
            role="Full Stack Developer"
            duration="Oct 2022 - May 2024"
            location="Remote"
            description={ unagiExperience }
          />
          <CompanyExperience
            company="Nación servicios"
            role="Full Stack Developer"
            companyLink="https://www.nacionservicios.com.ar/"
            duration="Mar 2022 - Oct 2022"
            location="Remote"
            description={ nacionExperience }
          />
          <CompanyExperience
            company="amalgama"
            role="Frontend Developer"
            duration="2021 - Mar 2022"
            location="Buenos Aires, Argentina"
            description={ amalgamaExperience }
          />
          <CompanyExperience
            company="Freelancer"
            role="Freelance Developer"
            duration="2019 - 2021"
            location="Remote"
            description={ freelancerExperience }
          />
        </ul>
      </div>
    </div>
  );
};

export default Experience;
