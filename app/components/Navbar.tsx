import React from 'react';
import Logo from './Logo';
import UnderlineLink from './UnderlineLink';
import InteractiveLogo from './InteractiveLogo';
import Image from 'next/image';
import ThemeSwitch from './ThemeSwitch';
import NavMobileMenu from './NavMobileMenu';

const Navbar = () => {
  return (
    <header className='w-full px-8 py-4 flex justify-between'>
      <NavMobileMenu />
      <Logo />
      <div className='flex justify-between items-center gap-6 w-full lg:hidden'>
        <nav className='flex justify-between items-center gap-6'>
          <UnderlineLink href="/">Home</UnderlineLink>
          <UnderlineLink href="/about">About</UnderlineLink>
        </nav>
        <nav className='flex justify-between items-center gap-3'>
          <InteractiveLogo href="https://linkedin.com/in/ramiro-montiel" >
            <Image src="/linkedin.png" alt="linkedin" width={ 32 } height={ 32 }/>
          </InteractiveLogo>
          <InteractiveLogo href="https://github.com/ramaMont">
            <Image src="/github.png" alt="github" width={ 32 } height={ 32 } className='dark:invert'/>
          </InteractiveLogo>
          <InteractiveLogo href="https://gitlab.com/ramaMont">
            <Image src="/gitlab.png" alt="gitlab" width={ 32 } height={ 32 }/>
          </InteractiveLogo>
          <ThemeSwitch />
        </nav>
      </div>
    </header>
  );
};

export default Navbar;
