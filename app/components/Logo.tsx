'use client';
import React from 'react';
import Link from 'next/link';
import { motion } from 'framer-motion';

const CustomLink = motion(Link);

function Logo () {
  return (
    <CustomLink
      href='/'
      className='w-16 h-16 bg-dark text-light flex items-center justify-center rounded-full text-2xl font-bold dark:border-2 dark:border-solid dark:border-light absolute left-[50%] top-2 translate-x-[-50%]'
      whileHover={ {
        backgroundColor: ['#1b1b1b', 'rgba(131,58,180,1)','rgba(253,29,29,1)','rgba(252,176,69,1)','rgba(131,58,180,1)', '#1b1b1b'],
        transition: {
          duration: 1,
          repeat: Infinity
        },
        transitionEnd: {
          backgroundColor: '#1b1b1b'
        }
      } }
    >
        RIM
    </CustomLink>
  );
}

export default Logo;
