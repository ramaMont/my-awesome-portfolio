'use client';

import React, { useContext, useRef } from 'react';
import { LayoutRouterContext } from 'next/dist/shared/lib/app-router-context.shared-runtime';

type FrozenRouteProps = {
    children: React.ReactNode;
}

const FrozenRoute : React.FC<FrozenRouteProps> = ({ children }) => {
  const context = useContext(LayoutRouterContext);
  const frozen = useRef(context).current;

  return <LayoutRouterContext.Provider value={ frozen }>{ children }</LayoutRouterContext.Provider>;
};

export default FrozenRoute;
