'use client';
import React from 'react';
import { motion } from 'framer-motion';

type InteractiveLogoProps = {
    children: React.ReactNode;
    href: string;
};

const InteractiveLogo : React.FC<InteractiveLogoProps> = ({ children, href }) => {
  return (
    <motion.a
      href={ href }
      className='w-6'
      target='_blank'
      whileHover={ { y:-2 } }
      whileTap={ { scale:0.9 } }
    >
      { children }
    </motion.a>
  );
};

export default InteractiveLogo;
