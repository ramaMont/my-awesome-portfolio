import Image from 'next/image';
import devImg from '../public/images/developer.png';
import Share from '../public/share.svg';
import Layout from './components/Layout';
import AnimatedText from './components/AnimatedText';
import Link from 'next/link';

export default function Home () {
  return (
    <Layout className='md:!pt-0'>
      <div className='flex items-center justify-between w-full lg:flex-col'>
        <div className='w-1/2 md:w-full'>
          <Image
            className='w-full h-auto lg:hidden md:inline-block'
            src={ devImg }
            alt="Developer"
            width={ 400 }
            height={ 400 }
            sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 50vw'
            priority
          />
        </div>
        <div className='w-2/5 flex flex-col items-center self-center gap-4 lg:w-full lg:text-center'>
          <AnimatedText text="Turning ideas Into Reality With Code" className='!text-6xl !text-left xl:!text-5xl lg:!text-center lg:!text-6xl md:!text-5xl sm:!text-3xl' />
          <p className='text-lg md:text-sm'>
            As a skilled full-stack developer, I am dedicated to turning ideas into innovative web applications.
            Let&#39;s talk so together we can bring to life your ideas and make them a reality.
          </p>
          <div className='w-full flex gap-4 justify-start items-center lg:justify-center'>
            <Link
              href={ process.env.RESUME_URL as string }
              target='_blank'
              className='group flex gap-2 items-center bg-dark text-light p-2.5 px-6 rounded-lg text-lg font-semibold border-2 border-solid border-transparent hover:bg-light hover:text-dark hover:border-dark md:p-2 md:px-4 md:text-base'
            >
              Resume
              <Image src={ Share } className='filter invert group-hover:invert-0' alt='share icon' width={ 20 } height={ 20 } />
            </Link>
            <Link
              href='mailto:tec.rimontiel@gmail.com'
              target='_blank'
              className='text-lg font-medium capitalize relative group md:text-base'
            >
              Contact
              <span className='h-1 inline-block  absolute left-0 -bottom-0.5 bg-dark w-0 group-hover:w-full transition-[width] ease duration-300 dark:bg-light'>
                &nbsp;
              </span>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  );
}
