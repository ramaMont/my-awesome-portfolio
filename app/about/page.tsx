import Image from 'next/image';
import AnimatedText from '../components/AnimatedText';
import Layout from '../components/Layout';
import profile from '../../public/images/initial portfolio image.jpeg';
import AnimatedNumber from '../components/AnimatedNumber';
import Skills from '../components/Skills';
import Experience from '../components/Experience';
import Education from '../components/Education';

export default function About () {
  const yearsOfExperience = new Date().getFullYear() - 2020;

  return (
    <Layout>
      <div className="w-full flex flex-col justify-center gap-16 md:gap-8">
        <AnimatedText text="Crafting My Story" className='lg:!text-7xl sm:!text-6xl xs:!text-4xl' />
        <div className='grid w-full grid-cols-8 gap-16 sm:gap-8'>
          <div className='col-span-3 flex flex-col items-start justify-start gap-4 xl:col-span-4 md:order-2 md:col-span-8'>
            <h2 className='text-lg font-bold uppercase text-dark/75 dark:text-light/75'>
              Biography
            </h2>
            <p className='font-medium'>
              Hey there, I&apos;m Ramiro, a versatile Full Stack Developer blending my expertise in frontend technologies, especially React, with a strong
              academic foundation in backend development. With a career spanning 4 years, I&apos;m dedicated to crafting seamless digital experiences
              that marry beauty with functionality. Let&apos;s bring your visions to life through innovative solutions.
            </p>
            <p className='font-medium'>
              I understand the value of listening to my clients&apos; needs and translating their ideas into tangible solutions. My approach goes beyond
              aesthetics; it&apos;s about delivering precisely what my clients envision, ensuring their satisfaction with every project.
            </p>
            <p className='font-medium'>
              Be it a website, mobile app, or any digital endeavor, I seamlessly integrate frontend and backend technologies to deliver fully functional
              solutions. My agile methodology, coupled with industry best practices, ensures projects are not only operational but also optimized for success.
              I&apos;m eager to apply my expertise and passion to elevate your next project.
            </p>
          </div>
          <div className='col-span-3 h-max relative rounded-2xl border-2 border-solid border-dark bg-light p-8 dark:bg-dark dark:border-light xl:col-span-4 md:order-1 md:col-span-8'>
            <Image
              src={ profile }
              alt="Ramiro"
              className='w-full h-auto rounded-2xl'
              sizes='(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'
              priority
            />
            <div className='absolute top-0 -right-3 -z-10 w-[102%] h-[103%] rounded-[2rem] bg-dark dark:bg-light' />
          </div>
          <div className='col-span-2 grid grid-cols-1 grid-rows-3 items-center xl:col-span-8 xl:flex xl:flex-row xl:justify-between xl:items-center md:order-3'>
            <div className='row-span-1 flex flex-col items-end justify-center xl:items-center'>
              <AnimatedNumber numberValue={ yearsOfExperience } />
              <h2 className='text-xl font-medium capitalize text-dark/75 dark:text-light/75 xl:text-center md:text-lg sm:text-base xs:text-sm'>
                Years of Experience
              </h2>
            </div>
            <div className='row-span-1 flex flex-col items-end justify-center xl:items-center'>
              <AnimatedNumber numberValue={ 12 } />
              <h2 className='text-xl font-medium capitalize text-dark/75 dark:text-light/75 xl:text-center md:text-lg sm:text-base xs:text-sm'>
                Projects completed
              </h2>
            </div>
          </div>
        </div>
        <Skills />
        <Experience />
        <Education />
      </div>
    </Layout>
  );
}
