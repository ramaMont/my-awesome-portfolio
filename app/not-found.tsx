import Link from 'next/link';
import Layout from './components/Layout';

export default function NotFound () {
  return (
    <Layout>
      <div className='flex flex-col items-center justify-center gap-8'>
        <h2>Not Found</h2>
        <p>Could not find requested page</p>
        <button>
          <Link className='hover:underline' href="/">Return Home</Link>
        </button>
      </div>
    </Layout>
  );
}
