import React from 'react';

import type { Metadata } from 'next';
import { Montserrat } from 'next/font/google';
import './globals.css';
import Navbar from './components/Navbar';
import PageAnimatePresence from './components/PageAnimationPresence';
import { Providers } from './Providers';
import { Analytics } from '@vercel/analytics/react';
import { SpeedInsights } from '@vercel/speed-insights/next';

const montserrat = Montserrat({ subsets: ['latin'], variable: '--font-mont' });

export const metadata: Metadata = {
  title: 'My portfolio',
  description: 'RIM Portfolio'
};

export default function RootLayout ({
  children
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={ `${montserrat.className} bg-light dark:bg-dark` }>
        <Analytics />
        <SpeedInsights />
        <Providers>
          <Navbar />
          <PageAnimatePresence>
            { children }
          </PageAnimatePresence>
        </Providers>
      </body>
    </html>
  );
}
