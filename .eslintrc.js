module.exports = {
  extends: 'next/core-web-vitals',
  rules: {
    // General rules
    'no-console': 'warn',
    'no-debugger': 'warn',
    'no-alert': 'warn',
    'no-unused-vars': ['error', { args: 'all', argsIgnorePattern: '^_' }],
    'no-var': 'error',
    'prefer-const': 'error',
    'prefer-template': 'error',
    'no-use-before-define': 'error',
    'no-undef': 'error',
    'no-underscore-dangle': 'off', // Depending on your preference

    // Stylistic rules
    indent: ['error', 2, { SwitchCase: 1 }],
    quotes: ['error', 'single', { avoidEscape: true }],
    semi: ['error', 'always'],
    'comma-dangle': ['error', 'never'],
    'object-curly-spacing': ['error', 'always'],
    'array-bracket-spacing': ['error', 'never'],
    'arrow-parens': ['error', 'always'],
    'arrow-spacing': ['error', { before: true, after: true }],
    'linebreak-style': ['error', 'unix'],
    'no-trailing-spaces': 'error',
    'no-multiple-empty-lines': ['error', { max: 1 }],
    'space-before-function-paren': ['error', 'always'],

    // React-specific rules
    'react/prop-types': 'off', // If you're not using PropTypes
    'react/react-in-jsx-scope': 'off', // Not needed in React 17+
    'react/jsx-uses-react': 'off', // Not needed in React 17+
    'react/jsx-uses-vars': 'error',
    'react/jsx-curly-spacing': ['error', { when: 'always', children: true }],
    'react/jsx-wrap-multilines': ['error', { prop: 'ignore' }],
    'react/jsx-fragments': ['error', 'syntax'],
    'react/jsx-closing-bracket-location': [
      'error'
    ]
  }
};
